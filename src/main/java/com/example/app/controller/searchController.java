package com.example.app.controller;

import com.example.app.Exceptions.RecordNotFoundException;
import com.example.app.allResponse.Response;
import com.example.app.model.GeoLocation;
import com.example.app.repo.SearchRepo;
import com.example.app.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/geolocation")
public class searchController {

    @Autowired
    private SearchService searchService;
    @Autowired
    private SearchRepo searchRepo;

    @GetMapping("/{address}")
    public ResponseEntity<Object> getLocationByAddress(@PathVariable String address) throws IOException {
        String add=address;

        GeoLocation geoLocationfromDb = searchService.getLocationByAddress(address);
        // if the  location not exist in our database
        if(geoLocationfromDb==null || geoLocationfromDb.equals(""))
        {
            // call external api to get the location.
          String location= searchService.callGecodeStreetApi(address);
          // the address not exist also on the external api.
            if(location==null || location.equals(""))
            {
                throw new RecordNotFoundException(" this address not found in DB and by calling external api : " + add);
            }
            else {
                //    then insert it on our DB.
                GeoLocation geoLocation = new GeoLocation();
                geoLocation.setAddress(address);
                geoLocation.setLocation(location);
                searchService.addLocation(geoLocation);

                Response response = new Response("get this location for this address is success from external Api", HttpStatus.OK.toString(), location);
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }
        Response response = new Response("get this location for this address is success from our DB", HttpStatus.OK.toString(), geoLocationfromDb);
        return new ResponseEntity<>(response, HttpStatus.OK);



    }
    @PostMapping("/sendNotification")
    public ResponseEntity<Object> addAddress(@RequestParam String email,@RequestParam String location) throws MessagingException {

        if(!email.equals("")){
            searchService.sendNotification(email,location);
            Response response = new Response("send email was success", HttpStatus.CREATED.toString(), "success");
            return new ResponseEntity<>(response, HttpStatus.OK);

        }
        Response response = new Response("send email  is fault", HttpStatus.BAD_REQUEST.toString(), null);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}