package com.example.app.service;

import com.example.app.Exceptions.RecordNotFoundException;
import com.example.app.model.GeoLocation;
import com.example.app.repo.SearchRepo;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

@Service
public class SearchService {

    @Autowired
    public SearchRepo searchRepo;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${opencage.api.key}")
    private String apiKey;

    public GeoLocation getLocationByAddress(String address) {

        GeoLocation geoLocation = searchRepo.findByAddress(address);
        System.out.println("the geoLocation is "+geoLocation);
        return geoLocation;

    }

    public GeoLocation addLocation(GeoLocation newLocation) {
        return searchRepo.save(newLocation);
    }

    @Async
    public void sendNotification(String email,String location) throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setTo(email);
        helper.setSubject("GeoLocation App");
        helper.setText("<html><body>"
                + "Thank you for using our application to get the location of your address <br><br>"
                + "The Location is : " + location + "<br><br><br>"
                + "Best regards,<br>"
                + "The GeoLocation App Team"
                + "</body></html>", true);

        javaMailSender.send(message);
    }


    public String callGecodeStreetApi(String address) throws IOException {

        String url = "https://api.opencagedata.com/geocode/v1/json?key=" + apiKey +
                "&q=" + address;

        // call api using rest template.
        String results = restTemplate.getForObject(url, String.class);

        if (results != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode response = objectMapper.readTree(results);

            if (response.has("results") && response.get("results").isArray()) {
                // get the first result from the full json response.
                JsonNode firstResult = response.get("results").get(0);
                if (firstResult.has("bounds") && firstResult.get("bounds").has("northeast")) {
                    JsonNode boundsNode = firstResult.get("bounds").get("northeast");
                    // get this position from the full json response.
                    double latitude = boundsNode.get("lat").asDouble();
                    double longitude = boundsNode.get("lng").asDouble();
                    String location= ""+latitude+" , "+longitude+"";
                    return location;
                }
            }
        }
        return null;
    }

}