package com.example.app.allResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Response {

    private String message;

    private String statusCode;

    private  Object responseObj;


    public Response(HttpStatus statusCode) {

    }
    public Response(String message, String statusCode) {

    }

    public ResponseEntity<Object> Response(String message, HttpStatus statusCode, Object responseObj) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("message", message);
        map.put("status", statusCode.value());
        map.put("data", responseObj);

        return new ResponseEntity<Object>(map,statusCode);
    }


}
