package com.example.app.repo;

import com.example.app.model.GeoLocation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SearchRepo extends JpaRepository<GeoLocation,Long>{

   GeoLocation findByAddress(String address);
}
